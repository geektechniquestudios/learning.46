package com.geektechnique.ldap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")//guide inconsistency, get vs request mapping
    public String index() {
        return "Welcome to the home page!";
    }
}
